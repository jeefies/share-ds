---
layout: cover
---

## 树上问题和分治

---

利用 `dfn` 的经典问题。

<v-clicks>

- 子树加，点加，子树和，点和。
- 换根子树最小值
- 点加，链求和
- 链加，链求和
- ……

</v-clicks>

---

[[SCOI2015]情报传递](https://www.luogu.com.cn/problem/P4216)

给定树，需要支持：

- 让一个点从当前时刻开始，每秒操作点权 $+1$
- 查询一条链中有多少点的点权大于 $c$

<v-click>

离线变成点加链求和即可。

</v-click>

[『MdOI R1』Path](https://www.luogu.com.cn/problem/P6072)

选择两条简单路径，满足没有重合的点，且边权异或和的**加和**最大。

<v-click>

一定存在一个点，使的两条路径一个在子树内，一个在子树外。子树内可以树上启发式合并求，但是子树外？

考虑找到全局最大的那个异或对，那么只有两个点的祖先不为这个对，于是相当于我们只求一条链的答案，这是不难的。

</v-click>

---

[[BJOI2014]大融合](https://www.luogu.com.cn/problem/P4219)

<v-click>

考虑离线，先把这棵树建出来，每个点维护当前连通状态下子树大小，于是相当于链加点求即可。

</v-click>


[[HDU 5709] Claris Loves Painting](https://acm.hdu.edu.cn/showproblem.php?pid=5709)

询问一个点的子树中与其距离不超过 $d$ 的点有多少种不同的点权。

<v-click>

树上启发式合并，每个颜色维护最浅出现深度，问题变成了有多少颜色出现最浅深度 $\le d$，用一棵平衡树维护即可。也可以线段树合并。

</v-click>

[Can Bash Save the Day?](https://www.luogu.com.cn/problem/CF757G)

一棵 $n$ 个点的树和一个排列 $p_i$，边有边权，支持两种操作：
- `l r x`，询问 $\sum_{i=l}^{r} dis(p_i, x)$。
- `x`，交换 $p_x, p_{x+1}$。

<v-click>

$1$ 操作每次把 $p_i$ 到根​的路径上 $+1$，主席树 + 链剖分维护即可。

对于 $2$ 操作，可以发现只会改变第 $i$ 棵主席树的值，所以暴力重建即可。

</v-click>

---

### 链分治

链分治分为重链，长链，实链。

对于路径统计问题，有一种思路是：钦定一个根变为有根树，并且枚举LCA。

在加入信息之前，我们计算新信息与旧信息之间的贡献，不难发现他们代表的路径的 LCA 一定是当前点。

如果加入的复杂度和子树大小相关,这就是**重链剖分**。

如果加入的复杂度和子树最大深度相关,这就是**长链剖分**。

如果树是动态链接的，这就是**实链剖分**。

树上启发式合并维护实际上是重链剖分，根据 lxl 所讲，也就是树上扫描线。

---

[Blood Cousins](https://www.luogu.com.cn/problem/CF208E)

<v-click>

长链剖分板子。

</v-click>

[[WC2010] 重建计划](https://www.luogu.com.cn/problem/P4292)

给出一棵带权树, 要求选出一条边数在 $[L,R]$ 内的路径,使得边权平均值最大。

<v-click>

首先 01 分数规划，问题就被转化为了：判定树上是否存在边数在 $[L,R]$ 内,且边权和大于 $0$ 的路径。

一个 `naive` 的想法是点分治，但是朴素的点分 + 线段树是 $O(n \log^2 n)$ 单次，虽然可以做到 $O(n \log n)$，但是仍然考虑长链剖分。在剖分时顺便维护一下答案即可。

</v-click>

---

[[Ynoi2011] ODT](https://www.luogu.com.cn/problem/P5314)

给你一棵树，边权为 $1$，有点权。

需要支持两个操作：

- `1 x y z`：表示把树上 $x$ 到 $y$ 这条简单路径的所有点点权都加上 $z$。
- `2 x y`：表示查询与点 $x$ 距离**小于等于** $1$ 的所有点里面的第 $y$ 小点权。

<v-click>

很好的套路，树剖，每个点对于轻儿子利用数据结构维护，重儿子和父亲单独不维护。

于是树剖实际上只影响了 $O(\log n)$ 个轻儿子，于是可以 $O(\log^2 n)$ 的修改了。

</v-click>

[[LNOI2014]LCA](https://www.luogu.com.cn/problem/P4211)

给出一个 $n$ 个节点的有根树（编号为 $0$ 到 $n-1$，根节点为 $0$）。

有 $m$ 次询问，每次询问给出 $l, r, z$，求 $\sum_{i=l}^r dep[\operatorname{LCA}(i,z)]$ 。

<v-click>

注意 $dep[\mathrm{LCA}(x, y)]$ 可以通过链加链求和的方式求出来。

扫描线 + 树剖即可。

</v-click>

---

[loj#6276.果树](https://loj.ac/p/6276)

树，点有颜色，求有多少链满足上面的颜色互不相同, 每种颜色在树上出现不超过 $20$ 次。

<v-click>

每种颜色可以 $O(C_i^2)$ 的去除排除答案，也就是一共 $20n$ 个矩阵，扫描线面积并，容斥一下即可。

</v-click>

给一棵二叉树，每个点有一个条件，条件形如 $< a，> a$，$a$ 是整数，每次询问从一个点 $x$ 开始，初始有一个数 $y$，如果 $y$ 满足 $x$ 节点的条件，则 $y$ 走到 $x$ 的左儿子，否则走到 $x$ 的右儿子，求最后走到哪个点。

<v-click>

这种向下找路径的题有一种通用的二分形式

对树进行树链剖分.向下每次走重链，如果重链可以走完，则走到一个叶子，否则二分在重链的哪个位置换到轻边。考虑一共至多切换 $O(\log n)$ 次，所以复杂度是 $O(n + m \log^2 n)$。

</v-click>

---

[CF1017G The Tree](https://www.luogu.com.cn/problem/CF1017G)

给定一棵树，维护以下3个操作：

1. 如果节点x为白色，则将其染黑。否则对这个节点的所有儿子递归进行相同操作。
2. 将以节点x为root的子树染白。
3. 查询节点x的颜色

<v-click>

树剖 + 重链上二分即可。

</v-click>

---

#### 实链剖分 LCT

LCT 的本质是一棵动态树，它支持在一个森林中动态加边删边，修改权值，维护路径信息等等很多很多功能。它和它依赖的另一个算法 Splay 都是由 Tarjan 老爷子提出的。

[[COI2009] OTOCI](https://www.luogu.com.cn/problem/P4312)

---

[[WC2006]水管局长](http://luogu.com.cn/problem/P4172)

只删边动态维护 MST

<v-click>

LCT 找环上最大边贪心的割掉即可。

</v-click>

[变化的道路](https://www.luogu.com.cn/problem/P4319)

删边加边动态维护 MST

<v-click>

线段树分治即可，注意是将 `link, cut` 回撤，而不是对于 `splay` 回撤。

</v-click>

---

前文只提到了 LCT 维护路径，然而在维护子树上似乎比较无力。

事实上，借助一些技巧，我们还是能维护一些子树信息的。(但是不能修改)

我们考虑维护一个“虚子树和”，即所有虚边儿子的权值和，这只需要在 access/link 里面修改，在 Splay 上可以视作这个点的“附加权值”来维护。

这要求贡献可逆。

[CF916E Jamie and Tree](http://luogu.com.cn/problem/CF916E)

有一棵 $n$ 个节点的有根树，标号为 $1 \sim n$，你需要维护以下三种操作

1. `1 v`：给定一个点 $v$，将整颗树的根变为 $v$。
2. `2 u v x`：给定两个点 $u,v$ 和整数 $x$，将 $\mathrm{lca}(u, v)$ 为根的子树的所有点的点权都加上 $x$。
3. `3 v`：给定一个点 $v$，你需要回答以 $v$ 所在的子树的所有点的权值和。

---

[遥远的国度](https://www.luogu.com.cn/problem/P3979)

- 换根
- 链修改
- 子树求最小值

<v-click>

什么？你问我为什么不树剖？（雾

由于此时信息不满足可减性，那么久不能 $O(1)$ 的维护了，但是可以来一个 $O(\log n)$ 的平衡树。

注意一个点，如果将这个平衡树变成 `splay`，那么复杂度不会多一只 $O(\log n)$。大概是因为这个 `splay` 和那个 `splay` 形态改变的是同步的？

但是为什么不树剖？

</v-click>

---

### 点分治

用于解决一类树上路径问题。

**核心思想**：每次钦定一个点,只统计经过该点的路径。然后各个子树之间便不再有贡献,可以分治。

在树上,只需要选取重心作为分治中心,则可以保证各个子任务的规模都不超过原问题的一半

[[IOI2011] Race](https://www.luogu.com.cn/problem/P4149)

<v-click>

板子题。

</v-click>

[Close Vertices](https://www.luogu.com.cn/problem/CF293E)

<v-click>

点分治 + 二维偏序。

</v-click>

[Jumping Monkey II](https://codeforces.com/gym/103495/problem/F)

---

### 边分治

点分治中，我们每次统计**只统计经过中心的路径**

边分治中，我们每次统计**只统计经过某条边的路径**

---

### 点/边分树

每次分治的时候，让各个子树的分治中心连接到当前中心上。这棵树的高度是 $O(\log n)$ 的。

不难发现，任意两个点 $x, y$ 的点分树上 LCA 一定在原树中两点的简单路径上。

而且在 $x, y$ 的公共祖先中, LCA 是唯一满足这个性质的。

[[HNOI2015] 开店](https://www.luogu.com.cn/problem/P3241)

<v-click>

实际上图已经三度化，所以边分治更简单，因为边分树是一颗二叉树。

建出边分树之后暴力跳父亲求贡献即可。

~~但是估计你们都会写点分而不是边分~~

</v-click>

---

[【模板】点分树 | 震波](https://www.luogu.com.cn/problem/P6329)

树上单点修改，每次查询到 $u$ 的距离不超过 $d$ 的点的权值和。

<v-click>

不修是简单的，但是修改是不简单的。

对每个分治中心，维护每颗子树的以深度为下标的权值前缀和。

可以树状数组，大小只需要开到深度即可。总空间 $O(n \log n)$。

询问时会在 $O(\log n)$ 层的分治块求和。需要求整体的和再减去自己子树内部的贡献,如果写边分治则无需考虑。

每次单点修改时,会修改 $O(\log n)$ 层的分治块，可以树状数组维护。

总复杂度 $O((n + m) \log^2 n)$。

</v-click>

[[CTSC2018] 暴力写挂](https://www.luogu.com.cn/problem/P4565)

<!-- 虽然边分治在查询的时候会少很多常数，但是边分治的点数边数本就带一个常数，所以两个抵了，效率也差不多。 -->

---

### 全局平衡二叉树

用于解决树链操作这一经典问题，显然是可以直接树剖做的，但是复杂度为 $O(n \log^2 n)$。

但是全局平衡二叉树一般操作的是到根的。据博客说，不到根的路径操作（区间而非前缀）如噩梦般难写，也就是它对于操作的限制很强，所以不如树剖。

于是这里略过，不讲。

