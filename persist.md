---
layout: cover
---

## 可持久化数据结构

---

线段树，平衡树，Trie……常用的就这些吧。

基本的就略了。

---

[[UNR #1] 火车管理](https://uoj.ac/problem/218)

有 $n$ 个栈，需要支持：

- 将 $x$ 加入 $[l, r]$ 的栈中
- $x$ 弹栈
- 求 $[l, r]$ 栈顶和

<v-click>

类似标记永久化，做到 $O(\log^2 n)$ 是不难的，考虑少一只 $\log n$。

每次入栈利用主席树标记永久化变成区间赋值，在求和的线段树上维护版本号，也就是时间戳。

线段树上查询单点栈顶元素入栈时间是可行的，然后在主席树上查询上一个时刻这个位置的值即可。

</v-click>

---

[[2017 山东三轮集训 Day6] C](https://loj.ac/p/6144)

<v-click>

每次 and 和 or 操作等价于将一些位强制设为 $0$ 或者 $1$。

如果赋值了一个没有赋值过的位就暴力重构一下可持久化 trie，这样的操作只会进行 $O(\log V)$ 次。

每次询问在对应的可持久化 trie 上二分即可。

</v-click>

[CF464E The Classic Problem](https://www.luogu.com.cn/problem/CF464E)

<v-click>

跑 DJK，但是松弛需要 $dis_y = dis_x + 2^k$，注意到可以利用可持久化的东西，先复制过来再赋值。

如何加法？二分出极长连续的一段 $1$，标记赋值即可。

</v-click>

[BZOJ 3551 Peaks]()

有 $n$ 个座山，其高度为 $h_i$。有 $m$ 条带权双向边连接某些山。多次询问，每次询问从 $v$ 出发只经过边权 $\le x$ 的边 所能到达的山中，第 $k$ 高的是多少。

<v-click>

显然 kruskal 重构树，但是需要可持久化平衡树启发式合并或者线段树合并或者 `01trie` 合并。

</v-click>

---

顺便复习一下 kruskal 重构树。

[[IOI2018] werewolf 狼人](https://www.luogu.com.cn/problem/P4899)

<v-click>

建立两棵重构树，那么问题转化为两个点构成的子树内是否存在公共点。

利用两者的 `dfn` 建立二维平面变成二维数点问题即可。

由于强制在线，所以可持久化即可。w.l

考虑类似一道题，只是带修：[Intersection of Permutations](https://wwuogu.com.cn/problem/CF1093E)。类似做即可。

</v-click>

---

[[SDOI2013]森林](https://www.luogu.com.cn/problem/P3302)

<v-click>

考虑静态的问题直接可持久化然后多个 `01trie` 上二分即可。

注意到只有加边，那么块的合并可以启发式合并，复杂度 $O(n \log^2 n)$。

现在最大的问题是连接两个点后还要高效的求出 LCA，一种做法是启发式合并的时候重构倍增数组，也有一种用 LCT 维护 LCA 的方法。

</v-click>

[「JOISC 2021 Day2」道路建设](https://www.luogu.com.cn/problem/P7561)

<v-click>

有一种转切比雪夫距离然后二分做到 $O(n \log^2 n)$ 时间，但是 $O(n)$ 空间的优秀做法。

然而这里考虑 $O(n \log n)$ 时间，但是空间是 $O(n \log n)$ 的优秀做法。

</v-click>

---

可持久化可并堆。

没啥题，也就用于求解 $k$ 短路问题。

略。

---

<img src="persist.png" style="max-width: 85%; display: inline;"></img>