---
layout: cover
---

## 线段树，平衡树

---

箕畚定义和原理，略。

---

### 分治信息

**定义**：可快速合并的信息

需要设计一套合理的标记来维护信息，这是与问题强相关的。

---

关押典狱长的典狱长：典中典

<v-clicks>

- 区间加/乘，求区间和
- 单点修，求区间最大子段和
- 给定若干形如 $ax + b > c$ 的不等式，支持删除/插入，求当 $x = k$ 时满足的不等式个数
- 区间加，求平均数，方差
- 单点修，区间相邻/所有数两两乘积的和
- 判断序列是否存在长度 $\ge 3$ 的等差子序列
- 判断子区间是否是一个等差数列
- ……

</v-clicks>

---

### 一些特殊的线段树

<v-clicks>

- **兔队线段树**：使用线段树维护前缀最大值的算法

经典的应用是楼房重建。

详细见：[从《楼房重建》出发浅谈一类使用线段树维护前缀最大值的算法](https://www.cnblogs.com/PinkRabbit/p/Segment-Tree-and-Prefix-Maximums.html)

- **分治线段树**：将删除利用 $O(\log m)$ 代价变为撤销的厉害做法

经典模板：[二分图 /【模板】线段树分治 ](https://www.luogu.com.cn/problem/P5787)

- **猫树**：利用 $O(\log n)$ 的代价预处理使得询问只需要合并一次信息。

</v-clicks>

---

[[HNOI2011]括号修复 / [JSOI2011]括号序列](https://www.luogu.com.cn/problem/P3215)

<v-click>

引入一个新的在部分情况下可以代替 `Splay` 的数据结构：`CMD Tree`。

详见：[# 一种轻量级平衡树](https://www.luogu.com.cn/blog/command-block/yi-zhong-qing-liang-ji-ping-heng-shu)

</v-click>

[T367829 软萌甜心小仙女](https://www.luogu.com.cn/problem/T367829)

<v-click>

注意到答案区间 $\le 3$。

</v-click>

---

- **李超线段树**：区间对一次函数取 $\max$，单点查。

这里来几个问题：

- $f_i = \max f_j + c_i \times c_j$，如果 $c_j$ 不连续，不单调？
- $f_i = \max_{i - k \le j \lt i} f_j + c_i \times c_j$，如果 $c_j$ 不连续，不单调？

---

小广告：[矩阵乘法与线段树标记](https://www.cnblogs.com/jeefy/p/17807239.html)

---

### 均摊复杂度问题

#### 颜色段均摊

反正 `ODT`！

判断是否可以均摊，我们可以看是否能够构造出一个操作序列使得序列复原，如果可以复原，那么基本是不可以均摊的。

或者我们看是否能找到一个量，不增，或者不减，或者有一个神秘的上界，再抽象一点说，存在某一个势能在我们可接受复杂度内。

---

[DZY Loves Colors](https://www.luogu.com.cn/problem/CF444C)

<v-click>

ODT + 线段树即可。

</v-click>

[CF453E - Little Pony and Lord Tirek](https://codeforces.com/problemset/problem/453/E)

<v-click>

ODT 推平顺便维护答案，利用可持久化即可。

</v-click>

[P5066 [Ynoi2014] 人人本着正义之名](https://www.luogu.com.cn/problem/P5066)

---

#### 容均摊

[#228. 基础数据结构练习题](https://uoj.ac/problem/228)

<v-click>
值域上的均摊：极差的减小
</v-click>

区间取 $\min, \max$？

<v-click>

维护对于最大值的非最大值的两套标记即可。均摊大概是利用值不同的个数，因为每次递归下去一定将某个节点的最大值和次大值合并了。

</v-click>

区间除法，开方，取模，变成 $\varphi(x)$，$\gcd(x, k)$？

<v-click>

对于前两者，维护最大值最小值，如果变化量相等，那么变成区间加减，注意到每次区间极差一定减小，对于除法变成 $\frac {\Delta}{x}$，对于开方 $\sqrt \Delta$。  
对于后三者，注意到每次值减半，那么每个位置只需要操作 $O(\log n)$ 次，每次随便二分即可。

</v-click>

---

[HDU 6315 Naive Operations](https://acm.hdu.edu.cn/showproblem.php?pid=6315)

给两个序列 $A$ 和 $B$，$B$ 是 $1 \sim n$ 的排列，需要支持：

- 对于 $A$ 区间 $+1$
- 求区间 $\sum \left\lfloor \frac {A_i} {B_i} \right\rfloor$。

<v-click>

注意到 $\sum \left\lfloor \frac {A_i} {B_i} \right\rfloor$ 是 $\sum \frac mi = O(m \log m)$，那么找到需要变化的位置暴力加的复杂度是正确的，也就是 $O(m \log m \log n)$。

</v-click>

---

[[Ynoi2007] rgxsxrs](https://www.luogu.com.cn/problem/P7447)

给定序列，需要支持：

- 将区间 $\gt x$ 的元素 $- x$。
- 区间和。

<v-clicks>

一类特殊的值域分块方式，倍增分块。  
分为 $[1, 2), [2, 4), [4, 8), \ldots, [2^k, 2^{k + 1})$ 种块，共 $O(\log V)$ 种。

</v-clicks>

---

### 树状数组

> 你是哪里冒出来的？

树状数组，常数巨小，一般维护支持差分的信息。

二分一个前缀信息，可以直接在 BIT 上二分，不需要在外面套一层，简单来说，从高位向低位确定即可。

#### 高阶前缀和（$k$ 次前缀和）

从区间加，区间求和开始，其实就是 $2$ 次前缀和。

发现如果在 $x$ 位置 $+1$，那么对于 $y \ge x$ 的位置的贡献是 $y - x + 1$。

注意到 $y - x + 1$ 是一个关于 $y$ 的多项式，考虑维护其系数即可。

---

如何扩展到高维。注意 DP 式子：

$$
f_{i, j} = f_{i, j - 1} + f_{i - 1, j}
$$

实际上是一个格路径问题，也就是在 $x$ 位置上 $+1$，那么对于 $y \ge x$ 的位置的贡献为 $\binom {y - x + k - 1} {k - 1}$。

例如在 $k = 3$ 时，式子为 $y^2 + (3 - 2x) y + x^2 - 3x + 2$。

考虑到这是一个关于 $y$ 的 $k$ 次多项式，于是得到了一个 $O(k q \log n)$ 的做法。

---

<img src="seg-img.png" style="max-width: 55%; display: inline;"></img>