---
layout: cover
---

## 支配

---

一般来说求 $i, j$ 间的贡献，此时可能存在形如：

- $i, j, k$ 如果 $i, j$ 有贡献，那么 $k$ 无贡献。
- $i, j$ 有贡献，那么 $k$ 贡献需要满足xxx条件。
- ……

大概就是说利用性质，将可能成为答案的 $(i, j)$ 减少到 $O(n)$ 或者 $O(n \log n)$ 级别。

---

[CF522D Closest Equals](https://www.luogu.com.cn/problem/CF522D)

<v-click>

注意到如果 $i, j, k$ 分别有贡献，那么 $i, k$ 贡献一定不优，所以可能贡献对是 $O(n)$ 的。

</v-click>

[Souvenirs](https://www.luogu.com.cn/problem/CF765F)

<v-click>

如果是 $a_i < a_j < a_k$，则 $(i, k)$ 的意义被 $(i, j)$ 掩盖。

考虑 $a_i > a_j$, $a_j < a_k$，$a_i \lt a_k$，那么 $(i, k)$ 能做贡献当且仅当 $a_k - a_i \lt a_i - a_j$，也就是 $a_i \gt \frac {a_k + a_j}{2}$，注意到差值减半，那么可能的支配对有 $O(n \log n)$ 个。

</v-click>

[CodeChef MINXORSEG](https://exam.codechef.com/problems/minxorseg)

每次求区间 $[l, r]$ 中最小的 $i \ne j, a_i \oplus a_j$。

<v-click>

还是只有 $O(n \log n)$ 个支配对，考虑 `LCP` 即可找到。

</v-click>

---

[[Ynoi2006] rldcot](https://www.luogu.com.cn/problem/P7880)

给定一棵 $n$ 个节点的树，树根为 $1$，每个点有一个编号，每条边有一个边权。

定义 $dep(x)$ 表示一个点到根简单路径上边权的和，$lca(x,y)$ 表示 $x,y$ 节点在树上的最近公共祖先。

共 $m$ 组询问，每次询问给出 $l,r$，求对于所有点编号的二元组 $(i,j)$ 满足 $l \le i,j \le r$ ，有多少种不同的 $dep(lca(i, j))$。

<v-click>

我们考虑树上启发式合并的过程，每次我们在将轻儿子向重儿子合并的时候，同时对重儿子维护一棵平衡树找到轻儿子中每个点的前驱后继，最后再将这个轻儿子中所有点插入平衡树，即可以保证不重不漏，而且真正有贡献的点对个数也减少到了启发式合并的复杂度，即 $O(n \log n)$。

<img src="https://cdn.luogu.com.cn/upload/image_hosting/5g913ugf.png" style="max-width: 35%">

</v-click>

---

<img src="control.png" style="max-width: 75%; display: inline;"></img>