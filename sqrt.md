---
layout: cover
---

## 根号算法

$$\Huge{\text{stO lxl Orz}}$$

$$\large\text{stO CMD Orz}$$

$$\text{stO fsfdgdg Orz}$$

---
layout: cover
---

### 分块

---

在数据结构问题中，我们需要维护一些离散的信息单位，单位的数目会影响处理的复杂度。

如果信息支持不受限制的合并与批量处理，那么就可以使用线段树等区间数据结构。

当然，有时并不存在高效的信息合并化简方式，而需要在“批量”和“零散”之间找到**平衡点**，这就是分块的核心思想。

这一类问题的性质更弱，所以问题形式变化多端，“整散”关系多种多样，具有传统数据结构难以比拟的灵活性。

---

[P4168 [Violet]蒲公英](https://www.luogu.com.cn/problem/P4168)

区间众数，强制在线。
  
[P5048 [Ynoi2019 模拟赛] Yuno loves sqrt technology III](https://www.luogu.com.cn/problem/P5048)

区间众数，强制在线。要求空间 $O(n)$。

<v-click>

正常做法空间复杂度在 $cnt_{i, x}$ 这个前缀和上，它用于判定散块中某个数在询问区间中的出现次数是否 $>k$。

变成用 `vector` 记录下同类数的所有出现位置查询之后的第 $k$ 个的位置即可。

空间复杂度改进为 $O(n)$ ，时间常数也减小了。

</v-click>

---

[P5046 [Ynoi2019 模拟赛] Yuno loves sqrt technology I](https://www.luogu.com.cn/problem/P5046)

区间逆序对，强制在线。
  
在逆序对问题中，每一对数都有贡献，贡献模式是二维的。

询问时，答案的贡献成分如下图 :

<img src="https://cdn.luogu.com.cn/upload/image_hosting/o0wwd4cf.png" style="max-width: 30%;">

绿色是整块内部的贡献，橙色是散块内部的贡献，红色是整块和散块之间的贡献，蓝色是散块之间的贡献。

分别 $O(\sqrt n)$ 的直接做或者预处理即可。

---

### 经典分块技巧

一些 基于分块的技巧 和 分块需要的技巧。

- **块状数组**

某些分块可以视作三层的 $\sqrt{n}$ 叉树，如图。

![](https://cdn.luogu.com.cn/upload/image_hosting/zt60wpkw.png?x-oss-process=image/resize,m_lfit,h_200)

---

块状数组查询和修改的复杂度一优一劣，用于查询和修改数目不同的问题，以平衡复杂度。

- $O(1)$ 单点修改，$O(\sqrt{n})$ 查询区间和。

  维护块和，修改时只需修改对应元素以及块和，查询时和数列分快相同。
  
- $O(\sqrt{n})$ 单点修改，$O(1)$ 查询前缀和。

  维护块内前缀和，块和的前缀和。查询时整块正缀和加上块内前缀和。
  
  修改时暴力 $O(\sqrt{n})$ 分别重新计算块内前缀和和块和的前缀和。

---
  
当值域大小为 $O(n)$ 时，可以用类似权值线段树的形式维护 “权值 $\sqrt{n}$ 叉树”。这个技巧被称为**值域分块**。
  
- $O(1)$ 插入一个数，$O(\sqrt{n})$ 查询第 $k$ 小。

  维护块和。查询时先逐块确定，然后进入块内逐个确定。
  
- $O(\sqrt{n})$ 插入一个数，$O(1)$ 查询第 $k$ 小。（较为困难）

  首先求出答案在那个值域块内，再求答案。
  
  对每个 $k$ 维护第 $k$ 大值在哪个值域块中，会形成 $O(\sqrt{n})$ 个段。
  
  一次修改后，每个段的端点会移动，总复杂度是 $O(\sqrt{n})$ 的。
  
  对于每个值域块维护一个有序序列，利用插入排序可以做到 $O(\sqrt{n})$。
  
  再维护每个块前面的数的数目，将 $k$ 减去之后直接取对应下标即可。   

---
  
- **例题** : [CC Chef and Churu](https://vjudge.net/problem/CodeChef-FNCS)

  **题意** : 给定一个长度为 $n$ 的序列和 $m$ 个区间。
  
  有两种操作：

  - 把序列中的第 $x$ 个数改为 $y$
  - 求第 $x$ 个区间到第 $y$ 个区间的区间和的和

<v-click>

将区间分块。维护 $c[k][i]$ 表示在第 $k$ 块中第 $i$ 个位置被覆盖了多少次。

这样，在单点修改之后，能 $O(\sqrt{n})$ 地计算出各个块的和的变化。

查询时，对于散块暴力 $O(\sqrt{n})$ 次区间求和，使用 $O(\sqrt{n})$ 修改 $O(1)$ 求和的分块即可。

</v-click>

---

**配合莫队使用**：注意到莫队一共是 $O(n \sqrt m)$ 次修改，$O(m)$ 次查询，所以可以通过一种 $O(1) - O(\sqrt n)$ 的数据结构来平衡复杂度。

<v-click>

**块状链表**

解决一系列带插入的序列问题。（在带插入的序列上维护分块）

将某个元素插入某个块时，将该块重构。若某个块的大小超过 $2B$ （或其他自定常数）则分裂为两个大小为 $B$ 的块。

这样，分裂次数是 $O(n/B)$ 的，且能保证各个块的大小在 $[B,2B]$ 之间。

</v-click>
<v-click>

[P4278 带插入区间K小值](https://www.luogu.com.cn/problem/P4278)

</v-click>
<v-click>

维护序列块的前缀权值块状数组，差分能得到任意一端块的权值块状数组。再将散块 $O(B)$ 建立一个权值块状数组。

在线性组合后的权值块状数组上，模仿主席树二分来爬，复杂度是 $O(\sqrt{n})$ 的。

在单点修改时，只会有 $O(n/B)$ 次 $O(1)$ 的权值块状数组更新。

取 $B=\sqrt{n}$ 即可做到 $O(n\sqrt{n})$。

</v-click>

---

[P3793 由乃救爷爷](https://www.luogu.com.cn/problem/P3793)

静态序列，查询区间 $\min$，需要 $O(n) - O(1)$。

<v-click>

该问题的一个经典做法是 $\rm ST$ 表，但需要 $O(n\log n)$ 的预处理时间。

考虑将原序列分为 $O(\log n)$ 一块，每一块预处理前/后缀 $\min$ ，整块的 $\min$ 上建立 $\rm ST$ 表。

这样，若查询的区间端点异块，则使用散块前/后缀 $\min$ 拼上一段整块的 $\min$ 就能做到 $O(1)$ 查询。

上面的复杂度是 $O(n)-O(1)$ 的。

若区间端点同块，只能暴力，但是题目中一般不会出现这种情况。这样，我们就得到了一个简易的 $O(n)-O(1)\ \rm rmq$ 替代品。

虽然实际上单次最坏复杂度是 $O(\log n)$ 的，如果愿意，可以在底层套一个线段树，做到单次 $O(\log \log n)$ 最坏，不过没有啥必要。

</v-click>

---

### 根号分治 & 自然根号

有时，根号复杂度会产生于一些非常自然的问题中，出现所谓**根号分治**。

例 : 有若干数**和**为 $n$ ，则最多有 $\frac na$ 个数字大于 $a$。

「**Be Surrounded**」

维护一个 $n$ 个点 $m$ 条边的无向图，支持下列两种操作 :
  
  - 将点 $u$ 的权值 $+y$
  - 查询与 $u$ 相连的点的权值和
  
<v-click>

点度数总和是 $O(m)$ 的，对度数进行根号分治。

对于度数 $\geq \sqrt{m}$ 的点，称为大点，反之称为小点。

在查询小点时暴力。修改时主动给周围的大点贡献，由于大点数目是 $O(\sqrt{m})$ 的，一次修改也不会超过 $O(\sqrt{m})$。

总复杂度 $O(q\sqrt{m})$。

</v-click>

---

 [P3396 哈希冲突](https://www.luogu.com.cn/problem/P3396)

维护一个长为 $n$ 的序列 $A$，支持下列操作 :
  
  - 单点修改
  - 查询 $\sum\limits_{i=1}^n[i\bmod x=y] A_i$

<v-click>
  
考虑 $x\geq \sqrt{n}$ 的询问，显然可以 $O(\sqrt{n})$ 计算。

对于剩下每个询问 $(x,y)$ ，都维护其答案。在单点修改时，对于每个 $x$ 都有一个 $y$ 的答案改变，复杂度也是 $O(\sqrt{n})$。

</v-click>

---

- 有若干数和为 $n$ ，则最多有 $O(\sqrt{n})$ 个不同的数。

- 有 $m$ 个集合，大小分别为 $S_1,S_2...S_m$。设 $n=\sum_{i=1}^mS_i$。
  
有 $q$ 次对子询问，处理询问 $(x,y)$ 的复杂度为 $O\big(\min(S_x,S_y)\big)$。

若将询问记忆化，则总复杂度为 $O(n\sqrt{q})$。
  
**证明** : 我们选出复杂度最大的 $q$ 个不同的询问进行求和。
  
记 $S$ 从大到小排序后的结果为 $S'$ ，复杂度最大的 $q$ 个不同的询问的复杂度和为 $O\Big(\sum_{i=1}^{\sqrt{q}}S_i\times i\Big)$。

最差情况是 $S$ 为 $O(\sqrt{q})$ 个 $O(n/\sqrt{q})$ ，此时复杂度为 $O(n\sqrt{q})$。

---

### 空间消失术 - 逐块处理

逐块处理，如果对询问，每个块的答案之间相对独立，则我们可以对每个块统计每次询问其对答案的贡献。

一般可以空间做到 $O(n)$，并且常数更小。

[[Ynoi2018] 五彩斑斓的世界](https://www.luogu.com.cn/problem/P4117)

<v-click>

设当前操作为减 $x$ ，当前最大值为 $mx$

- 若 $mx \ge 2x$，则可以将 $[1, x]$ 的数整体 $+x$ ，然后整体 $-x$

- 若 $mx \lt 2x$，则暴力将 $[x + 1, mx]$ 的数 $−x$

势能分析下来每个块是 $O(n)$ 的，空间也是 $O(n)$ 的。注意到空限 `64MB`，但是每个块的答案之间相对独立，所以逐块处理即可。

</v-click>

---

### 分块后分治

如果分治时带离散化，维护 pair 形式的贡献，则合并两个大小为 $n$ 的子问题时会产生 $O(n^2)$ 个 pair。

对于形如 $T(n) = 2T(\frac n 2) + O(n^2)$ 的复杂度，我们可以先将序列分为 $\sqrt n$ 大小的块，然后每个块进行上述分治。

[[Ynoi2013] D2T2](https://www.luogu.com.cn/problem/P5611)

给一个长为 $n$ 的序列，有 $m$ 次查询操作。

查询操作形如 $l, r, L, R$，表示将序列中值在 $[L, R]$ 内的位置保留不变，其他的位置变成 $0$ 时，序列中 $[l, r]$ 区间内的最大子段和，这个子段可以是空的。

<v-click>

显然的是 $O(n^2 + n + q)$ 是简单的，但是不太能过。

发现 $n^2$ 与 $n+q$ 并不平衡，我们需要通过分块来让它们平衡，也就是分块后分治。

</v-click>

---
layout: cover
---
  
### 莫队

---


简单莫队都会……

[[Ynoi2016]这是我自己的发明](https://www.luogu.com.cn/problem/P4689)

<v-click>

注意到信息具有可减性，那么差分，变成若干若干个区间的贡献加加减减的，跑莫队即可。

</v-click>

区间逆序对，考虑莫队。

<v-click>

莫队二次离线即可。

</v-click>

---

树上莫队，变成欧拉序跑莫队即可。

不放题了。

#### 莫队 + bitset

K 区间颜色数：给一个序列，每次给出k个区间，求 k 个区间的并内有多少个种出现过的值。

莫队 + bitset 直接做，即可 $O(\frac {nm} w)$。

[[Ynoi2016] 掉进兔子洞](https://www.luogu.com.cn/problem/P4688)

<v-click>

先考虑如果每个数不相同怎么做，莫队跑出每个区间的值域 bitset，然后 & 起来，这些就是被删掉的
数了。

相同？想办法把相同的变成不同的，发现将第 $k$ 个出现的 $x$ 离散化成 $x + k - 1$ 即可。

这样还是只有 $O(n)$ 种数。

</v-click>

---

[[Ynoi2011] WBLT](https://www.luogu.com.cn/problem/P5313)

<v-click>

当 $b$ 比较大的时候，可以考虑莫队维护出区间的 bitset，初始 $B$ 全 $1$，通过不断右移 & $B$，这样当 $B = 0$ 时就找到答案了。

这个复杂度是 $O(\frac {nm}{b})$ 的，当 $b \le w$ 的时候就炸了。

对于比较小的 $b$，可以对每个 $b$ 跑一次莫队，那么对于得出的 bitset，在上面找 $\rm mex$ 即可。

这是 $O(n \sqrt {bm} + \frac {nm}{w})$ 的，所以总复杂度 $O(\frac {nm}{w})$。

</v-click>

---

### 树分块

然后树分块是个什么东西？

因为一般只需要查链信息，所以树分块可以直接随机撒点，在树上随机撒 $\frac nS$ 个点，关键点间期望距离不超过 $S$。优势很明显，当 $S = \sqrt n$ 时，可以枚举点对预处理，然后跳根号次就可以跳出一条路径。

最正确的是 `topology cluster partition`，预处理的代码都很长了，不如上期望。

<img src="https://img.imgdb.cn/item/607504b88322e6675c225e15.png" style="max-width: 35%">

---

[未知提交题]()

强制在线，查询链颜色数。

<v-click>

然后随便来一个链加，链求和。

[[Ynoi2009] rpdq](https://www.luogu.com.cn/problem/P6778)

每次给定 $(l, r)$，求：

$$
\sum_{l \le x \lt y \le r} dis(x, y)
$$

</v-click>
<v-click>

转化为：

$$
\begin{aligned}
&\frac 1 2 \sum_{l \le x \le r} \sum_{l \le y \le r} dis(x, y) \\
= & \frac 1 2 (F(r, r) + F(l - 1, l - 1) - F(l - 1, r) - F(r, l - 1))
\end{aligned}
$$

其中 $F(l, r) = \sum_{1 \le x \le l} \sum_{1 \le y \le r} dis(x, y)$

</v-click>

---

然后考虑莫队，变成 $O(n \sqrt m)$ 次求：

$$
\sum_{y \le r} dis(x, y)
$$

扫描线，但是由于只有 $O(n)$ 次加法，所以需要考虑树分块平衡复杂度。

注意树分块更容易实现的点加链和，所以类似树状数组维护二阶前缀和的思路即可。

