---
layout: cover
---

## 扫描线

---

一般来说，对于二维问题，扫描线就是一条在平面上移动的线，不断维护信息。

通过扫描线可以将静态的二维问题转换为动态的一维问题。

一般可以通过差分减少问题的自由度。

---

雷电劈上电线：电中电

<v-clicks>

- 矩形面积并，窗口的星星
- 若干矩形，每次查询一个点被多少矩形包含
- 给定序列，查询区间中有多少不同的数，查询区间中有多少值只出现一次
- 多次独立查询，如果将区间 $[l,r]$ 中所有数都 $+1$，那么整个序列有多少个不同的数？
- ……

</v-clicks>

---

### 区间子区间

一般来说套一个历史和/历史最值什么的。

[Pudding Monsters](https://www.luogu.com.cn/problem/CF526F) / [Good Subsegments](https://www.luogu.com.cn/problem/CF997E)

<v-click>

利用单调栈维护区间 $\max - \min + 1$，然后乱做即可

</v-click>

[[NOIP2022] 比赛](https://www.luogu.com.cn/problem/P8868)

<v-click>

标记维护一下做即可。

</v-click>

---

### 换维扫描线

有的问题直接按照时间顺序不好处理，可以考虑扫描线序列维，数据结构时间维。

![](https://gitlab.com/jeefies/image-repo/uploads/6e286e08ab0998fe3e080056df5a8c57/202311302051793.png)

使得 区间加 $\to$ 单点加，单点查 $\to$ 区间查

---

[[Ynoi Easy Round 2021] TEST_152](https://www.luogu.com.cn/problem/P8512)

<v-click>

利用 ODT 变成若干矩形加减，然后扫描线即可。

</v-click>

[[JOISC2021] 饮食区](https://loj.ac/p/3489)

[[IOI2021] 分糖果](https://loj.ac/p/3523)

[UR #19 前进四](https://uoj.ac/contest/51/problem/515)

<v-click>

类似的换维，发现相当于维护一个序列，需要支持：

- 区间取 $\min$
- 询问一个点被取 $\min$ 了多少次

于是就是 seg beats 了。

</v-click>

---

有一位精通 FFT 的大佬表示有过一些基于对时间倍增的暴力重构的想法。

简要地说，这是对于原本不易插入但多个结构易于合并查询结果的情况下的一种手段。但是如果重构的复杂度不似与时间相关的时候，就不妨把倍增改成分块。

例如经典的区间加，单点求值。

考虑一次修改，我们对其惰性执行，每发生 $b$ 次修改，才将当前修改池里的操作用到数组上，重新 $O(n)$ 做一遍前缀和。

对于一次询问，我们首先查询预处理部分的数组，再加上未进行的那 $O(b)$ 个修改对答案的贡献。

于是可以做到 $O(q \sqrt n)$，而应用的话：

动态 FFT：单点修改一个数列，对 FFT 后的数列单点查询。保证数列长度是 $2$ 的整次幂。

---

[未知来源的题](https://www.luogu.com.cn/user/3296)

定义一个点对 $(i, j)$ 是好的当且仅当：$\exists k \in N^{*}, \left \lfloor \frac{i}{k} \right \rfloor = j$。

给定一棵 $n$ 个点的树，树上点的权值互不相同，每次询问给定一个路径 $(x,y)$，问路径上多少个点对 $(u, v)$ 满足 $u \ne v$ 且 $(a_u, a_v)$ 是好的。$1 \le n, m, q \le 5 \times 10^4$。

<v-click>

树上扫描线。

其实对于每一个 $x$ 可以 $O(\sqrt m)$ 预处理出可以与那些点形成贡献。路径可以拆出 $O(\log^2 n)$ 个矩形贡献。

于是二维数点即可，复杂度 $O(q \log^3 n + n \sqrt m \log n)$。

</v-click>

---


<img src="scan.png" style="max-width: 100%; display: inline;"></img>