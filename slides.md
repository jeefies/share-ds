---
layout: cover
theme: slidev-theme-academic
title: DS 乱讲
favicon: favicon.ico
transition: slide-left
drawings:
  syncAll: false
  persist: true
---

# DS 乱讲

---

<Toc maxDepth="2"></Toc>

---
src: ./basic.md
---

---
src: ./seg.md
---

---
src: ./segseg.md
---

---
src: ./scan.md
---

---
src: ./control.md
---

---
src: ./persist.md
---

---
src: ./tree.md
---

---
src: ./sqrt.md
---